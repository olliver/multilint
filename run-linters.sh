#!/bin/sh
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

set -eu

DOCKER_ARGS="--rm --volume \"$(pwd):/workdir\" --workdir \"/workdir\""
CHECKMAKE_ARGS=" "
CHECKMAKE_REPO="registry.hub.docker.com/mandrean/checkmake:latest"
FLAKE8_ARGS="--max-line-length 100 --show-source --statistics"
FLAKE8_REPO="registry.hub.docker.com/pipelinecomponents/flake8:latest"
HADOLINT_ARGS="--format tty"
HADOLINT_REPO="registry.hub.docker.com/hadolint/hadolint:latest-debian"
LUACHECK_ARGS="--quiet"
LUACHECK_REPO="registry.hub.docker.com/thibf/luacheck:latest"
PYLINT_ARGS="--output-format=colorized"
PYLINT_REPO="registry.hub.docker.com/cytopia/pylint:latest"
SHELLCHECK_ARGS="--color=always --format=tty --shell=sh --external-sources"
SHELLCHECK_REPO="registry.hub.docker.com/koalaman/shellcheck:stable"
YAMLLINT_ARGS="--config-data '{extends: default, rules: {document-start: {present: false}, line-length: {max: 120}}}'"
YAMLLINT_REPO="registry.hub.docker.com/pipelinecomponents/yamllint:latest"


usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo "Run this repositories linters"
	echo "    -d   Run hadolint on Dockerfiles"
	echo "    -f   Run flake8 on python files"
	echo "    -h   Print usage"
	echo "    -l   Run luacheck on lua scripts"
	echo "    -m   Run checkmake on Makefiles"
	echo "    -n   Do not use docker for checks"
	echo "    -o   Always run offline"
	echo "    -P   Run pylint and flake8 on python files"
	echo "    -p   Run pylint on python files"
	echo "    -s   Run shellcheck on shell scripts"
	echo "    -y   Run YAML lint"
	echo
	echo "Without any option, all linters are run."
}

_run_linter()
{
	_linter="${1%%_*}"
	_linter_repo="${2:?Missing argument to function}"
	_linter_args="${3:?Missing argument to function}"
	_linter_target="${4:?Missing argument to function}"
	_linter_version="${5:---version}"

	if [ "${1%%_docker-entry}" != "${1}" ]; then
		unset _docker_entry
	else
		_docker_entry="true"
	fi

	if [ -n "${docker:-}" ] 1> "/dev/null"; then
		if [ -z "${offline:-}" ]; then
			docker pull "${_linter_repo}" 1> "/dev/null" || true
		fi
		_linter_cmd="docker run ${DOCKER_ARGS} ${_linter_repo} ${_docker_entry:+${_linter}}"
	elif command -v "${_linter}" 1> "/dev/null"; then
		_linter_cmd="${_linter}"
	else
		echo "Neither '${_linter}' nor 'docker' are installed to run the linter."
		return
	fi

	eval "${_linter_cmd}" "${_linter_version}"

	if [ -d "${_linter_target##\*}" ]; then
		if ! eval "${_linter_cmd}" "${_linter_args}" "${_linter_target}"; then
			exit_fail=1
		fi
	elif [ -f "${_linter_target}" ]; then
		if ! eval "${_linter_cmd}" "${_linter_args}" "${_linter_target}"; then
			exit_fail=1
		fi
	else
		for _ext in ${_linter_target}; do
			_iname="${_iname:+${_iname} -o} -iname '*${_ext}'"
		done
		eval find '.' -type f "${_iname}" | while read -r _target_file; do
			echo "Linting '${_target_file}'."
			if ! eval "${_linter_cmd}" "${_linter_args}" "${_target_file}"; then
				echo "  No problems detected with '${_target_file}'."
			fi
		done
	fi
}

run_yamllint()
{
	_run_linter "yamllint" \
	            "${YAMLLINT_REPO}" \
		    "${YAMLLINT_ARGS}" \
		    "${1:-.yaml .yml}"
}

run_flake8()
{
	_run_linter "flake8_docker-entry" \
	            "${FLAKE8_REPO}" \
		    "${FLAKE8_ARGS}" \
		    "${1:-.py}"
}

run_pylint()
{
	_run_linter "pylint_docker-entry" \
	            "${PYLINT_REPO}" \
		    "${PYLINT_ARGS}" \
		    "${1:-.py}"
}

run_shellcheck()
{
	_run_linter "shellcheck_docker-entry" \
	            "${SHELLCHECK_REPO}" \
		    "${SHELLCHECK_ARGS}" \
		    "${1:-.sh}"
}

run_luacheck()
{
	_run_linter "luacheck" \
	            "${LUACHECK_REPO}" \
		    "${LUACHECK_ARGS}" \
		    "${1:-.lua}"
}

run_hadolint()
{
	_run_linter "hadolint" \
	            "${HADOLINT_REPO}" \
		    "${HADOLINT_ARGS}" \
		    "${1:-Dockerfile .Dockerfile}"
}

run_checkmake()
{
	_run_linter "checkmake_docker-entry" \
	            "${CHECKMAKE_REPO}" \
		    "${CHECKMAKE_ARGS}" \
		    "${1:-Makefile .mk}"
}

main()
{
	docker="$(command -v "docker")"

	while getopts ":dfhlmnoPpsy" options; do
		case "${options}" in
		d)
			run_linters="${run_linters:-} run_hadolint"
			;;
		f)
			run_linters="${run_linters:-} run_flake8"
			;;
		h)
			usage
			exit 0
			;;
		l)
			run_linters="${run_linters:-} run_luacheck"
			;;
		m)
			run_linters="${run_linters:-} run_checkmake"
			;;
		n)
			unset docker
			;;
		P)
			run_linters="${run_linters:-} run_pylint run_flake8"
			;;
		p)
			run_linters="${run_linters:-} run_pylint"
			;;
		o)
			offline="true"
			;;
		s)
			run_linters="${run_linters:-} run_shellcheck"
			;;
		y)
			run_linters="${run_linters:-} run_yamllint"
			;;
		:)
			echo "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			echo "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	case "${0##*/}" in
	checkmake.sh)
		run_linters="run_checkmake"
		;;
	flake8.sh)
		run_linters="run_flake8"
		;;
	hadolint.sh)
		run_linters="run_hadolint"
		;;
	luacheck.sh)
		run_linters="run_luacheck"
		;;
	pythonlint.sh)
		run_linters="run_pylint run_flake8"
		set +e
		echo "Warning: exit code will not reflect status of all linters!"
		;;
	pylint.sh)
		run_linters="run_pylint"
		;;
	shellcheck.sh)
		run_linters="run_shellcheck"
		;;
	yamllint.sh)
		run_linters="run_yamllint"
		;;
	*)
		if [ -z "${run_linters:-}" ]; then
			run_linters="
				run_hadolint
				run_shellcheck
				run_luacheck
				run_checkmake
				run_yamllint
				run_pylint
				run_flake8
			"
		fi
	esac

	for linter in ${run_linters}; do
		${linter} "${1:-}"
	done

	exit "${exit_fail:-0}"
}

main "${@}"

exit 0
