# Multi-linter
A little bit like github's mega linter, but different

## Install
Just copy the script to your path (`~/bin` might be a good idea), optionally
add symlinks to the binary, e.g. `ln -s "shellcheck.sh" "~/bin/run-linters.sh"`
to only run a specific linter.
